## Parallel Reduction Example

### Usage

```bash
mkdir build && cd build
cmake .. -DGTEST_LIBRARY=<path/to/gtest/lib> -DGTEST_INCLUDE_DIR=<path/to/gtest/include> -DGTEST_MAIN_LIBRARY=<path/to/gtest/lib>
cmake --build .
```