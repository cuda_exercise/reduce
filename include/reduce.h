#pragma once

#define BLOCK_SIZE 256

namespace prim
{

template <typename T>
T reduce(T* data, 
         size_t n, 
         int type, 
         int numBlocks);

} // namespace prim
