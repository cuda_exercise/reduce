#include <iostream>
#include <gtest/gtest.h>

#include "reduce.h"

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

// kahan summation algorithm, from https://github.com/NVIDIA/cuda-samples/blob/master/Samples/2_Concepts_and_Techniques/reduction/reduction.cpp
template <class T>
T reduceCPU(T *data, int size) {
  T sum = data[0];
  T c = (T)0.0;

  for (int i = 1; i < size; i++) {
    T y = data[i] - c;
    T t = sum + y;
    c = (t - sum) - y;
    sum = t;
  }

  return sum;
}

TEST(Reduce, TestKernel1) {

    size_t n = 1 << 24;
    float* data= (float*)malloc(sizeof(float) * n);

    // set seed
    srand(0);
    for (int i = 0; i < n; i++) {
        data[i] = (rand() & 0xFF) / (float)RAND_MAX;
    }

    int numBlocks = (n + BLOCK_SIZE - 1) / BLOCK_SIZE;

    int type = 8;
    if (type >= 8) {
        numBlocks = 4096;
    }

    float result = prim::reduce<float>(data, n, type, numBlocks);

    std::cout << result << std::endl;

    float sum = reduceCPU<float>(data, n);

    // float 数据类型求和目前会存在精度问题，因此这里的精度要求放宽
    EXPECT_NEAR(result, sum, 10);
    
    
}