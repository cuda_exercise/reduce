#include <iostream>
#include <stdio.h>
#include <cuda_runtime.h>

#include "reduce.h"
#include "helper_timer.h"


namespace prim
{

template <typename T>
__global__ void reduce_kernel_0(const T* data, const size_t n, T* result) {

    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    if(tid >= n) return;

    atomicAdd(&result[0], data[tid]);
}

template <typename T, unsigned int blockSize>
__global__ void reduce_kernel_1(const T* data, const size_t n, T* result) {

    __shared__ T sdata[blockSize];

    int tid = threadIdx.x;
    int globalId = threadIdx.x + blockIdx.x * blockDim.x;
    sdata[tid] = globalId >= n ? 0 : data[globalId];
    
    __syncthreads();

    for (int s = 1; s < blockDim.x; s *= 2) {
        if (tid % (2 * s) == 0) {
            sdata[tid] += sdata[tid + s];
        }
        __syncthreads();
    }

    if (tid == 0) {
        atomicAdd(&result[0], sdata[0]);
    }
}

// Save shared memory load  
template <typename T, unsigned int blockSize>
__global__ void reduce_kernel_2(const T* data, const size_t n, T* result) {

    __shared__ T sdata[blockSize];

    int tid = threadIdx.x;
    int globalId = threadIdx.x + blockIdx.x * blockDim.x;

    sdata[tid] = globalId >= n ? 0 : data[globalId];

    __syncthreads();

    T sum = sdata[tid];
    for (int s = 1; s < blockDim.x; s *= 2) {
        if (tid % (2 * s) == 0) {
            sdata[tid] = sum = sum + sdata[tid + s];
        }
        __syncthreads();
    }

    if (tid == 0) {
        atomicAdd(&result[0], sdata[0]);
    }
}

// Avoid control divergence
template <typename T, unsigned int blockSize>
__global__ void reduce_kernel_3(const T* data, const size_t n, T* result) {

    __shared__ T sdata[blockSize];

    int tid = threadIdx.x;
    int globalId = threadIdx.x + blockIdx.x * blockDim.x;

    sdata[tid] = globalId >= n ? 0 : data[globalId];

    __syncthreads();

    T sum = sdata[tid];
    for (int s = 1; s < blockSize; s *= 2) {
        int idx = 2 * s * tid;

        if(idx < blockSize) {
            sdata[idx] += sdata[idx + s]; 
            // sdata[idx] = sum = sum + sdata[idx + s];
        }
        __syncthreads();
    }

    if (tid == 0) {
        atomicAdd(&result[0], sdata[0]);
    }
}

// Avoid Bank Conflicts
template <typename T, unsigned int blockSize>
__global__ void reduce_kernel_4(const T* data, const size_t n, T* result) {

    __shared__ T sdata[blockSize];

    int tid = threadIdx.x;
    int globalId = threadIdx.x + blockIdx.x * blockDim.x;
    
    sdata[tid] = globalId >= n ? 0 : data[globalId];

    __syncthreads();

    T sum = sdata[tid];
    for (int s = blockSize / 2; s >= 1; s >>= 1) {
        if(tid < s) {
            sdata[tid] = sum = sum + sdata[tid + s];
        }
        __syncthreads();
    }

    if (tid == 0) {
        atomicAdd(&result[0], sdata[0]);
    }
}

// Loop Unrolling
template <typename T, unsigned int blockSize>
__global__ void reduce_kernel_5(const T* data, const size_t n, T* result) {
    __shared__ T sdata[blockSize];

    int tid = threadIdx.x;
    int globalId = threadIdx.x + blockIdx.x * blockDim.x;
    
    sdata[tid] = globalId >= n ? 0 : data[globalId];

    __syncthreads();

    T sum = sdata[tid];
    #pragma unroll
    for (int s = blockSize / 2; s >= 1; s >>= 1) {
        if(tid < s) {
            sdata[tid] = sum = sum + sdata[tid + s];
        }
        __syncthreads();
    }

    if (tid == 0) {
        atomicAdd(&result[0], sdata[0]);
    }

}


template <typename T, unsigned int blockSize>
__device__ void warpReduce(volatile T* sdata, int tid) {
    if(blockSize >= 64) sdata[tid] += sdata[tid + 32];
    if(blockSize >= 32) sdata[tid] += sdata[tid + 16];
    if(blockSize >= 16) sdata[tid] += sdata[tid + 8];
    if(blockSize >= 8) sdata[tid] += sdata[tid + 4];
    if(blockSize >= 4) sdata[tid] += sdata[tid + 2];
    if(blockSize >= 2) sdata[tid] += sdata[tid + 1];
}

// Last Warp Reduce
template <typename T, unsigned int blockSize>
__global__ void reduce_kernel_6(const T* data, const size_t n, T* result) {
    __shared__ T sdata[blockSize];

    int tid = threadIdx.x;
    int globalId = threadIdx.x + blockIdx.x * blockDim.x;
    
    sdata[tid] = globalId >= n ? 0 : data[globalId];

    __syncthreads();

    if(blockSize >= 1024) {
        if(tid < 512) {
            sdata[tid] += sdata[tid + 512];
        }
        __syncthreads();
    }

    if(blockSize >= 512) {
        if( tid < 256) {
            sdata[tid] += sdata[tid + 256];
        }
        __syncthreads();
    }

    if(blockSize >= 256) {
        if (tid < 128) {
            sdata[tid] += sdata[tid + 128];
        }
        __syncthreads();
    }

    if(blockSize >= 128) {
        if(tid < 64) {
            sdata[tid] += sdata[tid + 64];
        }
        __syncthreads();
    }

    if(tid < 32) {
        warpReduce<T, blockSize>(sdata, tid);
    }

    if (tid == 0) {
        atomicAdd(&result[0], sdata[0]);
    }
}

template <typename T, unsigned int blockSize>
__device__ void warpShuffleSum(T &sum, unsigned int mask) {
    
    if(blockSize >= 32) {
        sum += __shfl_down_sync(mask, sum, 16);    
    }
    if(blockSize >= 16) {
        sum += __shfl_down_sync(mask, sum, 8);
    }
    if(blockSize >= 8) {
        sum += __shfl_down_sync(mask, sum, 4);
    }
    if(blockSize >= 4) {
        sum += __shfl_down_sync(mask, sum, 2);
    }
    if(blockSize >= 2) {
        sum += __shfl_down_sync(mask, sum, 1);
    }
}

// Warp Shuffling 
template <typename T, unsigned int blockSize>
__global__ void reduce_kernel_7(const T* data, const size_t n, T* result) {
    __shared__ T sdata[blockSize];

    int tid = threadIdx.x;
    int globalId = threadIdx.x + blockIdx.x * blockDim.x;
    
    sdata[tid] = globalId >= n ? 0 : data[globalId];

    __syncthreads();

    if(blockSize >= 1024) {
        if(tid < 512) {
            sdata[tid] += sdata[tid + 512];
        }
        __syncthreads();
    }

    if(blockSize >= 512) {
        if( tid < 256) {
            sdata[tid] += sdata[tid + 256];
        }
        __syncthreads();
    }

    if(blockSize >= 256) {
        if (tid < 128) {
            sdata[tid] += sdata[tid + 128];
        }
        __syncthreads();
    }

    if(blockSize >= 128) {
        if(tid < 64) {
            sdata[tid] += sdata[tid + 64];
        }
        __syncthreads();
    }
    
    if(blockSize >= 64) {
        if(tid < 32) {
            sdata[tid] += sdata[tid + 32];
        }
        __syncthreads();
    }

    if (tid == 0) {
        T sum = sdata[0];
        warpShuffleSum<T, blockSize>(sum, 0xffffffff);
        atomicAdd(&result[0], sum);
    }

    
}

// Thread Coarsening
template <typename T, unsigned int blockSize>
__global__ void reduce_kernel_8(const T* data, const size_t n, T* result) {

    __shared__ T sdata[blockSize];

    int gridSize = blockSize * gridDim.x;
    int tid = threadIdx.x;
    int globalId = tid + blockIdx.x * blockSize;

    sdata[tid] = 0;

    T sum = sdata[tid];

    while(globalId < n) {
        sum += data[globalId];
        globalId += gridSize;
    }

    sdata[tid] = sum;

    __syncthreads();

    if(blockSize >= 1024) {
        if(tid < 512) {
            sdata[tid] = sum = sum + sdata[tid + 512];
        }
        __syncthreads();
    }

    if(blockSize >= 512) {
        if( tid < 256) {
            sdata[tid] = sum = sum + sdata[tid + 256];
        }
        __syncthreads();
    }

    if(blockSize >= 256) {
        if (tid < 128) {
            sdata[tid] = sum = sum + sdata[tid + 128];
        }
        __syncthreads();
    }

    if(blockSize >= 128) {
        if(tid < 64) {
            sdata[tid] = sum = sum + sdata[tid + 64];
        }
        __syncthreads();
    }

    if(blockSize >= 64) {
        if(tid < 32) {
            sdata[tid] += sdata[tid + 32];
        }
        __syncthreads();
    }

    if (tid == 0) {
        T sum = sdata[0];
        warpShuffleSum<T, blockSize>(sum, 0xffffffff);
        atomicAdd(&result[0], sum);
    }

}

template int reduce<int>(int* data, size_t n, int type, int numBlocks);
template float reduce<float>(float* data, size_t n, int type, int numBlocks);
template double reduce<double>(double* data, size_t n, int type, int numBlocks);

template <typename T>
T reduce(T* data, 
         size_t n, 
         int type, 
         int numBlocks) {
    T* d_data;
    T* d_result;
    T result[1] = {0};

    cudaMalloc((void**)&d_data, sizeof(T) * n);
    cudaMalloc((void**)&d_result, sizeof(T) * 1);

    cudaMemcpy(d_data, data, sizeof(T) * n, cudaMemcpyHostToDevice);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);
    int repeat = 100;
    for (int i = 0; i < repeat; i++) {
        cudaMemcpy(d_result, result, sizeof(T) * 1, cudaMemcpyHostToDevice);
        switch(type) {
            case 0:
                reduce_kernel_0<T><<<numBlocks, BLOCK_SIZE>>>(d_data, n, d_result);
                break;
            case 1:
                reduce_kernel_1<T, BLOCK_SIZE><<<numBlocks, BLOCK_SIZE>>>(d_data, n, d_result);
                break;
            case 2:
                reduce_kernel_2<T, BLOCK_SIZE><<<numBlocks, BLOCK_SIZE>>>(d_data, n, d_result);
                break;
            case 3:
                reduce_kernel_3<T, BLOCK_SIZE><<<numBlocks, BLOCK_SIZE>>>(d_data, n, d_result);
                break;
            case 4:
                reduce_kernel_4<T, BLOCK_SIZE><<<numBlocks, BLOCK_SIZE>>>(d_data, n, d_result);
                break;            
            case 5:
                reduce_kernel_5<T, BLOCK_SIZE><<<numBlocks, BLOCK_SIZE>>>(d_data, n, d_result);
                break;                            
            case 6:
                reduce_kernel_6<T, BLOCK_SIZE><<<numBlocks, BLOCK_SIZE>>>(d_data, n, d_result);
                break;                            
            case 7:
                reduce_kernel_7<T, BLOCK_SIZE><<<numBlocks, BLOCK_SIZE>>>(d_data, n, d_result);
                break;                                                            
            case 8:
                reduce_kernel_8<T, BLOCK_SIZE><<<numBlocks, BLOCK_SIZE>>>(d_data, n, d_result);
                break;
            default:
                std::cout << "Invalid type: " << type << std::endl;
                exit(1);
        }
    }
    cudaEventRecord(stop);

    cudaEventSynchronize(stop);

    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    std::cout << "Total time: " << milliseconds << "ms" << std::endl;

    size_t bytes = sizeof(T) * n * repeat;
    float seconds = milliseconds / 1000;
    float throughput = bytes / seconds / 1000 / 1000 / 1000;
    std::cout << "Throughput: " << throughput << "GB/s" << std::endl;

    size_t flops = n * repeat;      
    float gflops = flops / seconds / 1000 / 1000 / 1000;
    std::cout << "Performance: " << gflops << "GFLOPS" << std::endl;

    cudaMemcpy(result, d_result, sizeof(T) * 1, cudaMemcpyDeviceToHost);

    cudaFree(d_data);
    cudaFree(d_result);
    return result[0];

}    
} // namespace prim
